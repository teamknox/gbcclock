	.globl	.OAM

	.STAT		= 0x41	; Status
	.VBK		= 0x4F	; LCD RAM bank
	.BCPS		= 0x68	; BG write spec
	.BCPD		= 0x69	; BG write data
	.OCPS		= 0x6A	; OBJ write spec
	.OCPD		= 0x6B	; OBJ write data
	.SVBK		= 0x70	; Work memory bank

	.area	_CODE

_set_bkg_palette::
	PUSH	BC

	LDA	HL,7(SP)	; Skip return address and registers
	LD	B,(HL)		; BC = rgb_data
	DEC	HL
	LD	C,(HL)
	DEC	HL
	LD	E,(HL)		; E = nb_palettes
	DEC	HL
	LD	L,(HL)		; L = first_palette

	LD	A,E		; A = nb_palettes
	AND	#0xFF
	JR	NZ,1$
	POP	BC
	RET
1$:
	DEC	A
	AND	#0x07
	INC	A
	ADD	A		; A *= 8
	ADD	A
	ADD	A
	LD	E,A		; DE = nb_palettes
	XOR	A
	LD	D,A

        LD      A,L		; L = first_palette
	XOR	#0x07
	ADD	A,A		; A *= 8
	ADD	A,A
	ADD	A,A
	LD	L,A		; A = first BCPS data
2$:
	LDH	A,(.STAT)
	AND	#0x02
	JR	NZ,2$

	LD	A,L
	LDH	(.BCPS),A
	LD	A,(BC)
	LDH	(.BCPD),A

	INC	BC		; next rgb_data
	INC	HL		; next BCPS
	DEC	E
	LD	A,D
	OR	E
	JR	NZ,2$

	POP	BC
	RET

_set_sprite_palette::
	PUSH	BC

	LDA	HL,7(SP)	; Skip return address and registers
	LD	B,(HL)		; BC = rgb_data
	DEC	HL
	LD	C,(HL)
	DEC	HL
	LD	E,(HL)		; E = nb_palettes
	DEC	HL
	LD	L,(HL)		; L = first_palette

	LD	A,E		; A = nb_palettes
	AND	#0xFF
	JR	NZ,1$
	POP	BC
	RET
1$:
	DEC	A
	AND	#0x07
	INC	A
	ADD	A		; A *= 8
	ADD	A
	ADD	A
	LD	E,A		; DE = nb_palettes
	XOR	A
	LD	D,A

        LD      A,L		; L = first_palette
	XOR	#0x07
	ADD	A,A		; A *= 8
	ADD	A,A
	ADD	A,A
	LD	L,A		; A = first OCPS data
2$:
	LDH	A,(.STAT)
	AND	#0x02
	JR	NZ,2$

	LD	A,L
	LDH	(.OCPS),A
	LD	A,(BC)
	LDH	(.OCPD),A

	INC	BC		; next rgb_data
	INC	HL		; next OCPS
	DEC	E
	LD	A,D
	OR	E
	JR	NZ,2$

	POP	BC
	RET

.set_sprite_attr::
	LD	HL,#.OAM+3	; Calculate origin of sprite info

	SLA	C		; Multiply C by 4
	SLA	C
	LD	B,#0x00
	ADD	HL,BC

	LD	A,D		; Set sprite number
	LD	(HL),A
	RET

_set_sprite_attr::
	PUSH	BC

	LDA	HL,4(SP)	; Skip return address and registers
	LD	C,(HL)		; C = nb
	INC	HL
	LD	D,(HL)		; D = tile

	CALL	.set_sprite_attr

	POP	BC
	RET

# MBC3 RTC handling by GBC
This is a sample program for RTC embedded in the MBC3 cartridge. Try to extend it because the functions implemented are minimum. 

<table>
<tr>
<td><img src="./pics/clock.jpg"></td>
</tr>
</table>


# License
Copyright (c) Osamu OHASHI  
Distributed under the MIT License either version 1.0 or any later version. 

/********************************************************************
 * CLOCK                                                            *
 ********************************************************************/
#include <gb.h>

/* bitmaps */
#include "bkg.c"

unsigned char bkg_clear[] = {
        0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};
unsigned char msg_clock[] = { "GB-CLOCK" };
unsigned char msg_day[] =   { "DAY =" };
unsigned char msg_hor[] =   { "HOR =" };
unsigned char msg_min[] =   { "MIN =" };
unsigned char msg_sec[] =   { "SEC =" };

unsigned char msg_a_key[] = { "A: INIT TIME" };

UBYTE rtc_s;
UBYTE rtc_m;
UBYTE rtc_h;
UBYTE rtc_dh;
UBYTE rtc_dl;

void init_screen()
{
  int i;

  set_bkg_data(  0, 128, bkg );
  for( i=0; i<18; i++ ) {
    set_bkg_tiles( 0, i, 20, 1, bkg_clear );
  }
  set_bkg_tiles( 6, 1, 8, 1, msg_clock );
  set_bkg_tiles( 6, 3, 5, 1, msg_day );
  set_bkg_tiles( 6, 4, 5, 1, msg_hor );
  set_bkg_tiles( 6, 5, 5, 1, msg_min );
  set_bkg_tiles( 6, 6, 5, 1, msg_sec );
  set_bkg_tiles( 4, 10, 12, 1, msg_a_key );
  SHOW_BKG;
}

void get_day()
{
  UBYTE data;

  data = *(UBYTE *)0x6000;
  if( data & 0x1 ) {
    *(UBYTE *)0x6000 = 0;
  }
  *(UBYTE *)0x6000 = 1;
  data = *(UBYTE *)0x6000;
#if 0
  while( data != 0x3 ) {
    data = *(UBYTE *)0x6000;
  }
#else
  delay( 4UL );
#endif

  SWITCH_RAM_MBC1(0x0B);
  rtc_dl = *(UBYTE *)0xA000;
  SWITCH_RAM_MBC1(0x0C);
  rtc_dh = *(UBYTE *)0xA000;
  SWITCH_RAM_MBC1(0);
}

void get_time()
{
  SWITCH_RAM_MBC1(0x08);
  rtc_s = *(UBYTE *)0xA000;
  SWITCH_RAM_MBC1(0x09);
  rtc_m = *(UBYTE *)0xA000;
  SWITCH_RAM_MBC1(0x0A);
  rtc_h = *(UBYTE *)0xA000;
  SWITCH_RAM_MBC1(0);
}

void init_time()
{
  SWITCH_RAM_MBC1(0x08);
  *(UBYTE *)0xA000 = 0;
  SWITCH_RAM_MBC1(0x09);
  *(UBYTE *)0xA000 = 0;
  SWITCH_RAM_MBC1(0x0A);
  *(UBYTE *)0xA000 = 0;
  SWITCH_RAM_MBC1(0x0B);
  *(UBYTE *)0xA000 = 0;
  SWITCH_RAM_MBC1(0x0C);
  *(UBYTE *)0xA000 = 0;
  SWITCH_RAM_MBC1(0);
}

void print_date()
{
  UWORD day;
  unsigned char msg[3];

  day = ((UWORD)(rtc_dh&0x1))+((UWORD)(rtc_dl));
  msg[2] = (unsigned char)(day%10+0x30);
  day = day/10;
  msg[1] = (unsigned char)(day%10+0x30);
  day = day/10;
  msg[0] = (unsigned char)(day%10+0x30);
  set_bkg_tiles( 12, 3, 3, 1, msg );
}

void print_time()
{
  unsigned char msg[2];

  msg[0] = (unsigned char)(rtc_h/10+0x30);
  msg[1] = (unsigned char)(rtc_h%10+0x30);
  set_bkg_tiles( 12, 4, 2, 1, msg );

  msg[0] = (unsigned char)(rtc_m/10+0x30);
  msg[1] = (unsigned char)(rtc_m%10+0x30);
  set_bkg_tiles( 12, 5, 2, 1, msg );

  msg[0] = (unsigned char)(rtc_s/10+0x30);
  msg[1] = (unsigned char)(rtc_s%10+0x30);
  set_bkg_tiles( 12, 6, 2, 1, msg );
}


/*--------------------------------------------------------------------------*
 | main program                                                             |
 *--------------------------------------------------------------------------*/
void main()
{
  disable_interrupts();
  DISPLAY_OFF;

  init_screen();
  DISPLAY_ON;
  enable_interrupts();

  ENABLE_RAM_MBC1;

  while(1) {
    delay( 10UL );
    get_day();
    get_time();
    print_date();
    print_time();
    if( joypad() & J_A ) {
      init_time();
    }
  }
}
